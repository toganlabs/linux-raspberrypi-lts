#! /bin/bash

set -e
set -o pipefail

export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabi-

if [[ $# < 2 || $# > 3 ]]; then
    echo "Usage: $0 VERSION OUTDIR [MACHINE]" >&2
    exit 1
fi

VERSION=$1
OUTDIR=$2

if [[ $# == 3 ]]; then
    SELECTED_MACHINE=$3
else
    SELECTED_MACHINE=all
fi

do_checksums() {
    pushd $1 &> /dev/null
    md5sum *.xz > MD5SUMS
    sha1sum *.xz > SHA1SUMS
    sha256sum *.xz > SHA256SUMS
    popd &> /dev/null
}

do_build() {
    MACHINE=$1
    DEFCONFIG=$2

    # Output to a machine-specific subdir
    mkdir -p ${OUTDIR}/${MACHINE}

    # Clean
    make mrproper
    rm -rf img

    # Configure
    make $DEFCONFIG
    xz -c .config > ${OUTDIR}/${MACHINE}/config-${MACHINE}-${VERSION}.xz

    # Build images
    make -j8 Image
    make -j8 uImage
    xz -c arch/arm/boot/Image > ${OUTDIR}/${MACHINE}/Image-${MACHINE}-${VERSION}.xz
    xz -c arch/arm/boot/uImage > ${OUTDIR}/${MACHINE}/uImage-${MACHINE}-${VERSION}.xz

    # Grab the system map to aid debugging
    xz -c System.map > ${OUTDIR}/${MACHINE}/System-${MACHINE}-${VERSION}.map.xz

    # Build modules
    make -j8 modules
    make modules_install INSTALL_MOD_PATH=img
    rm -f img/lib/modules/*/{build,source}
    tar -C img -cvJf ${OUTDIR}/${MACHINE}/modules-${MACHINE}-${VERSION}.tar.xz lib/modules
    tar -C img -cvJf ${OUTDIR}/${MACHINE}/firmware-${MACHINE}-${VERSION}.tar.xz lib/firmware

    do_checksums ${OUTDIR}/${MACHINE}
}

if [[ "$SELECTED_MACHINE" == "raspberrypi" || "$SELECTED_MACHINE" == "all" ]]; then
    do_build raspberrypi bcmrpi_defconfig
fi

if [[ "$SELECTED_MACHINE" == "raspberrypi2" || "$SELECTED_MACHINE" == "all" ]]; then
    do_build raspberrypi2 bcm2709_defconfig
fi
