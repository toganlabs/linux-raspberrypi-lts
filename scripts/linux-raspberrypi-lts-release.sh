#! /bin/bash

set -e
set -o pipefail

if [[ $# != 1 ]]; then
    echo "Usage: $0 VERSION" >&2
    exit 1
fi
VERSION=$1

OUTDIR=release/rpi-${VERSION}
mkdir -p ${OUTDIR}

# Ensure release is tagged
TAGNAME=rpi-$VERSION
if [[ `git tag -l $TAGNAME` != "$TAGNAME" ]]; then
    echo "ERROR! Release must be correctly tagged!"
    exit 1
fi

do_checksums() {
    pushd $1 &> /dev/null
    md5sum *.xz > MD5SUMS
    sha1sum *.xz > SHA1SUMS
    sha256sum *.xz > SHA256SUMS
    popd &> /dev/null
}

do_src() {
    git archive --format=tar --prefix=linux-$TAGNAME/ $TAGNAME \
        | xz > ${OUTDIR}/linux-$TAGNAME.tar.xz

    do_checksums ${OUTDIR}
}

do_build() {
    ./scripts/linux-raspberrypi-lts-build.sh $VERSION $OUTDIR
}

do_src
do_build
